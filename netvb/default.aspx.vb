﻿Imports System.Data.SqlClient


Public Class _default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Session("login") IsNot Nothing) Then
            Response.Redirect("~/quienes.aspx")
        End If

        lblError.Visible = False
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MSSQL").ConnectionString)
        conn.Open()

        Dim query = String.Format("select id from usuarios where nombre='{0}' and pass='{1}';", txtNombre.Text, txtPass.Text)
        Dim cmd As New SqlCommand(query, conn)
        Dim reader = cmd.ExecuteReader()

        If reader.HasRows Then
            Dim data = reader.Read()

            Session.Add("login", reader.GetInt32(0))
            conn.Close()
            Response.Redirect("~/quienes.aspx")
        Else
            conn.Close()

            With lblError
                .Text = "Usuario o contraseña incorrectos"
                .Visible = True
            End With
        End If
    End Sub
End Class