﻿Public Class main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim file = System.IO.Path.GetFileName(Request.Url.AbsolutePath)

        linkCerrar.Visible = If((Session("login") Is Nothing), False, True)

        Select Case file
            Case "default.aspx"
                linkInicio.Enabled = False
            Case "quienes.aspx"
                linkQuienes.Enabled = False
        End Select
    End Sub

    Protected Sub linkCerrar_Click(sender As Object, e As EventArgs)
        linkCerrar.Visible = False
        Session.Remove("login")
        Response.Redirect("~/default.aspx")
    End Sub
End Class