﻿Imports System.Data.SqlClient

Public Class quienes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblFecha.Text = DateTime.Now.ToString("yyyy - MM - dd")

        If (Session("login") Is Nothing) Then
            Response.Redirect("~/default.aspx")
        End If

        sqlDs1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("MSSQL").ConnectionString
    End Sub

    Protected Sub tblMensajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles tblMensajes.RowDataBound
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            If (e.Row.RowState And DataControlRowState.Edit) > 0 Then
                Dim editstatus As Boolean = Convert.ToBoolean(e.Row.DataItem(3))
                Dim CheckBoxEditTemp As CheckBox = CType(e.Row.Cells(4).Controls(0), CheckBox)
                CheckBoxEditTemp.Checked = editstatus

                e.Row.Cells(4).DataBind()
            Else
                e.Row.Cells(4).Text = If(e.Row.DataItem(3), "SI", "NO")
            End If
        End If
    End Sub

    Private Sub addMsg(nombre, urgente, comentarios)
        Dim conn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("MSSQL").ConnectionString)
        conn.Open()

        Dim query = String.Format("insert into mensajes (fecha, nombre, urgente, comentarios) values (getdate(), '{0}', {1}, '{2}');", nombre, urgente, comentarios)
        Dim cmd As New SqlCommand(query, conn)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As SqlException
            Console.WriteLine(ex.ToString())
        End Try

        tblMensajes.DataBind()

        conn.Close()
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        addMsg(txtNombre.Text, CByte(chUrgente.Checked), txtCom.Text)
    End Sub

    Protected Sub DropDownList1_TextChanged(sender As Object, e As EventArgs)
        sqlDs1.SelectCommand = "SELECT * FROM mensajes where id=1;"
        tblMensajes.DataBind()
    End Sub

    Protected Sub btnPostback_Click(sender As Object, e As EventArgs)
        texto.Text = "Hola mundo"
    End Sub
End Class