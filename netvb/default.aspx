﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/main.Master" CodeBehind="default.aspx.vb" Inherits="netvb._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="css" runat="server">
    <style type="text/css">
        #tblForm {
            width: 20%;
            margin: 30px auto 0;
        }
        #tblForm td {
            text-align: center;
            padding: 5px 0;
        }
        #body_lblError{
            color: #F00;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <table id="tblForm">
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="Red" ID="valNombre" runat="server" ErrorMessage="Nombre es obligatorio" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>

                <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnLogin" runat="server" Text="Aceptar" OnClick="btnLogin_Click" />
            </td>
        </tr>
    </table>

</asp:Content>
