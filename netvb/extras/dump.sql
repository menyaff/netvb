USE [prueba]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 08/26/2020 21:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[pass] [varchar](64) NOT NULL,
 CONSTRAINT [PK_usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[usuarios] ON
INSERT [dbo].[usuarios] ([id], [nombre], [pass]) VALUES (1, N'User1', N'123')
INSERT [dbo].[usuarios] ([id], [nombre], [pass]) VALUES (2, N'User2', N'abc')
INSERT [dbo].[usuarios] ([id], [nombre], [pass]) VALUES (3, N'Usuario', N'entrar')
SET IDENTITY_INSERT [dbo].[usuarios] OFF
/****** Object:  Table [dbo].[mensajes]    Script Date: 08/26/2020 21:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mensajes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[nombre] [varchar](20) NULL,
	[urgente] [tinyint] NULL,
	[comentarios] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[mensajes] ON
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (1, CAST(0x0000A81301371091 AS DateTime), N'Manuel', 1, N'Lorem ipsum')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (2, CAST(0x0000A81A0048CCCB AS DateTime), N'Manuel', 1, N'Hola mundo')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (3, CAST(0x0000A81A0049927F AS DateTime), N'Meny', 0, N'Cosa')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (4, CAST(0x0000A81A004A011B AS DateTime), N'Erika', 1, N'NO')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (5, CAST(0x0000A81A004A2918 AS DateTime), N'Erika', 0, N'Aún no')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (6, CAST(0x0000A81A004A88A3 AS DateTime), N'Erika', 0, N'Nada de nada')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (7, CAST(0x0000A81A004DC70D AS DateTime), N'XXXXX', 1, N'...')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (8, CAST(0x0000A81A00FED174 AS DateTime), N'Manuel', 0, N'Sábado')
INSERT [dbo].[mensajes] ([id], [fecha], [nombre], [urgente], [comentarios]) VALUES (9, CAST(0x0000AA04012B4BAA AS DateTime), N'PRUEBA', 1, N'execute reader')
SET IDENTITY_INSERT [dbo].[mensajes] OFF
/****** Object:  Default [DF__mensajes__fecha__1920BF5C]    Script Date: 08/26/2020 21:57:02 ******/
ALTER TABLE [dbo].[mensajes] ADD  DEFAULT (getdate()) FOR [fecha]
GO
