﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/main.Master" CodeBehind="quienes.aspx.vb" Inherits="netvb.quienes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="js" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    QUIENES SOMOS?<br /><br />
    <asp:Label ID="lblFecha" runat="server" Text="Label"></asp:Label>
    <br /><br />
    <asp:DropDownList AutoPostBack="true" ID="DropDownList1" runat="server" OnTextChanged="DropDownList1_TextChanged">
        <asp:ListItem>Uno</asp:ListItem>
        <asp:ListItem>Dos</asp:ListItem>
    </asp:DropDownList>
    <asp:SqlDataSource ID="sqlDs1" runat="server"
        DeleteCommand="DELETE FROM mensajes where id = @id" 
        InsertCommand="INSERT INTO mensajes(fecha, nombre, urgente, comentarios) VALUES (CURRENT_TIMESTAMP,@nombre,@urgente,@comentarios)"
        UpdateCommand="UPDATE mensajes SET nombre = @nombre, urgente = @urgente, comentarios = @comentarios WHERE id = @id"
        SelectCommand="SELECT * FROM mensajes"
        ProviderName="System.Data.SqlClient"></asp:SqlDataSource>
    <asp:GridView ID="tblMensajes" runat="server" AllowSorting="True" DataSourceID="sqlDs1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>
    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
    <asp:CheckBox ID="chUrgente" runat="server" />
    <asp:TextBox ID="txtCom" runat="server" TextMode="MultiLine"></asp:TextBox>
    <asp:Button ID="btnAdd" runat="server" Text="Button" OnClick="btnAdd_Click" />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
    <asp:Button ID="btnPostback" runat="server" Text="Postback" OnClick="btnPostback_Click" />
    <asp:TextBox ID="texto" runat="server"></asp:TextBox>
</asp:Content>
